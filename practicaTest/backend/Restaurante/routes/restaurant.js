var express = require('express');
var router = express.Router();
const auth = require("../middleware/auth");

const { registerApi } = require('../middleware/logger-api');
const { restaurantController } = require('../controllers/restaurant.controller.js');


router.post('/RecibirPedidoDelCliente', auth, registerApi, restaurantController.reciveOrder);

router.get('/InformarEstadoDelPedidoAlCliente/:noOrden', auth, registerApi, restaurantController.getState);

router.post('/AvisarAlRepartidorQueYaEstaListoElPedido', auth, registerApi, restaurantController.markOrder);

router.get('/getOrders', auth, registerApi, restaurantController.getOrders);

router.get('/healthy', restaurantController.healthy);
module.exports = router;
