let chai = require("chai")
let chaiHttp = require("chai-http")
const app = require("../app")
const expect = require("chai").expect
chai.use(chaiHttp)


// unit test #1 - healthy api

describe("1. ms restaurante" , () =>{
    it("show healthy message" , (done) =>{
        chai.request(app).get('/restaurant/healthy').end(
            function (err , res) {
                if (err) console.log(err)
                expect(res).to.have.status(200);
                //expect(res.body).to.have.property('msg').equal("api ok!");
                expect(res.body).to.have.property('msg')
                done();
            }
        )
    })
})

//unit test #2 - recive order
describe('2. recive order', () => {
    it('show save order in array', (done) => {
        const status = 200
        const order = {
            username: 1,
            producto: "tamal"
        }
        chai.request(app)
            .post('/restaurant/RecibirPedidoDelCliente').set({
                "x-auth-token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyIjoidGVzdCJ9.yVhgeB8gk-MH5MIFP8n1GBQu8GbFN2zZCG7YRF8WU-E"
            })
            .send(order)
            .end(function(err, res){
                if(err) throw err; 
                expect(res).to.have.status(status);
                done()
            })
    })
});
