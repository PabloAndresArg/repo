const jwt = require('jsonwebtoken');

module.exports = function (req, res, next) {
    //GET TOKEN FROM HEADER
    const token = req.header('x-auth-token');    
    // check if not token
    if (!token) {
        return res.status(401).json({ msg: 'No token , authorization denied ' }); // codigo de no autorizado
    }
    // verify token
    try {
       jwt.verify(token, "sa_practica1");
       next();
    } catch (error) {
        console.log(error.message);
        res.status(401).json({ msg: 'Token is not valid' })
    }
}