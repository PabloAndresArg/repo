const axios = require("axios").default;

const doRequest = async (config) => {
    try {
        const response = await axios(config);
        return response.data;
    } catch (error) {
        console.log(error);
        return false;
    }
};
module.exports = {
    doRequest,
};
