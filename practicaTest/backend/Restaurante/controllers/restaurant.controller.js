const { doRequest } = require("../utils/doRequest");
let restaurantController = {};
var noOrders = 0;
var orders = [
    {
        "username": "test_user1",
        "producto": "pizza",
        "state": "en restaurante",
        "noOrden": 1
    }
];

restaurantController.reciveOrder = (req, res, next) => {
    if (req.body == undefined){
        return res.status(400).send({ msg: "error in save order"});
    } 
    noOrders++;
    orders.push({ ...req.body, state: "en restaurante", noOrden: noOrders });
    res.status(200).send({
        noOrden: noOrders
    });
}

restaurantController.getOrders = (req, res, next) => {
    res.status(200).send(
        orders
    );
}
restaurantController.healthy = (req, res, next) => {
    return res.status(200).send({ msg: 'api ok! :)' });
}

restaurantController.getState = (req, res, next) => {
    try {
        const order = orders.find((ord) => ord.noOrden == req.params.noOrden);
        const { state } = order;
        res.status(200).send({ state });
    } catch (error) {
        res.status(400).send({ msg: "error order not exist" });
    }
}


restaurantController.markOrder = async (req, res, next) => {
    let orderState;
    orders.forEach((ord) => {
        if (ord.noOrden == req.body.noOrden) {
            ord.state = "con repartidor";
            orderState = ord;
        }
    });
    let preUrl = process.env.REPARTIDOR_HOST || "http://34.70.179.197:5000";
    const url = preUrl + "/repartidor/recibe";
    const token = req.header('x-auth-token');

    const config = {
        url: url,
        method: "POST",
        data: {
            "id_pedido":orderState.noOrden,
            "token":token
        },
        headers: {
            "x-auth-token": token
        }
    };
    const fetchResponse = await doRequest(config);
    if (!fetchResponse) return res.status(400).json({ status: "error" });
/*     const fetch = require('node-fetch');
    fetch(url, {
        method: 'GET',
        headers: {
            "x-auth-token": token
        }
    }).then(res => res.json())
        .catch(error => console.error('Error:', error))
        .then(response => console.log('Success:', response)); */

    res.status(200).send(orderState);
}

module.exports = { restaurantController };