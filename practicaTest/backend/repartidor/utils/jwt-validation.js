const jwt = require("jsonwebtoken");

const validateJWT = (req, res, next) => {
    const token = req.body.token
    if (!token) {
        console.log('validation error')
        return res.status(401).json({ status: "error", msg: "There's no token..." });
    }
    jwt.verify(token, 'sa_practica1', function(err,data){
        if (err) {
            console.log('error jwt')
            return res.status(401).json({ status: "error", msg: "Invalid token" });
        }
        console.log('validation successful')
        next()
    })
};

module.exports = {
  validateJWT,
};