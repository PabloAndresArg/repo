let chai = require("chai")
let chaiHttp = require("chai-http")
const app = require("../app")
const expect = require("chai").expect

chai.use(chaiHttp)

//unit test #1 - api listening
describe('1. ms repartidor listening', () => {
    it('show hello world msg', (done) => {
        const status = 200
        chai.request(app)
            .get('/repartidor/hello')
            .end(function(err, res){
                if(err) throw err; 
                console.log(res.body)
                expect(res).to.have.status(status)
                //expect(res.body).to.have.property('msg').equal('hello world')
                expect(res.body).to.have.property('msg')
                done()
            })
    })
});

//unit test #2 - recive order
describe('2. recive order', () => {
    it('show save order in array', (done) => {
        const status = 200
        const order = {
            id_pedido: 1,
            token: "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyIjoidGVzdCJ9.yVhgeB8gk-MH5MIFP8n1GBQu8GbFN2zZCG7YRF8WU-E"
        }
        chai.request(app)
            .post('/repartidor/recibe')
            .send(order)
            .end(function(err, res){
                if(err) throw err; 
                console.log(res.body)
                expect(res).to.have.status(status)
                expect(res.body).to.have.property('msg').equal('pedido recibido')
                done()
            })
    })
});

//unit test #3 - info from order
describe('3. show info', () => {
    it('show order status', (done) => {
        const status = 200
        const order = {
            id_pedido: 1,
            token: "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyIjoidGVzdCJ9.yVhgeB8gk-MH5MIFP8n1GBQu8GbFN2zZCG7YRF8WU-E"
        }
        chai.request(app)
            .post('/repartidor/info')
            .send(order)
            .end(function(err, res){
                if(err) throw err; 
                console.log(res.body)
                expect(res).to.have.status(status)
                expect(res.body).to.have.property('state')
                done()
            })
    })
});

//unit test #4 - info from order
describe('4. deliver order', () => {
    it('update state from order', (done) => {
        const status = 200
        const order = {
            id_pedido: 1,
            token: "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyIjoidGVzdCJ9.yVhgeB8gk-MH5MIFP8n1GBQu8GbFN2zZCG7YRF8WU-E"
        }
        chai.request(app)
            .post('/repartidor/entrega')
            .send(order)
            .end(function(err, res){
                if(err) throw err; 
                console.log(res.body)
                expect(res).to.have.status(status)
                expect(res.body).to.have.property('msg').equal('pedido entregado')
                done()
            })
    })
});

//unit test #5 - info from order
describe('5. show orders', () => {
    it('show all orders', (done) => {
        const status = 200
        chai.request(app)
            .get('/repartidor/getData')
            .end(function(err, res){
                if(err) throw err; 
                console.log(res.body)
                expect(res).to.have.status(status)
                expect(res.body).to.be.an('array')
                done()
            })
    })
});