const { logger } = require('../utils/logger');
const { request } = require("express");

const registerApi = (req, res, next) => {
  const bodyToLog = {
    urlBase: req.baseUrl,
    method: req.method,
    body: req.body,
    headers: req.headers,
    url: req.url,
    originalUrl: req.originalUrl,
  };
  logger.info(bodyToLog);
  next();
};

module.exports = {
  registerApi,
};
