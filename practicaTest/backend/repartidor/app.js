var express = require('express')
const morgan = require('morgan')
const axios = require('axios');
const cors = require("cors");
const app = express()
app.use(cors());
app.set('port', process.env.PORT || 5000)
app.use(express.json())
app.use(morgan('dev'))
app.use('/repartidor',require('./controller/route'))

module.exports = app