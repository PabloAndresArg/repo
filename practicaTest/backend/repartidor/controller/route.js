const { Router } = require('express')
const router = Router()

const repartidor_controller = require('./controller')
const { validateJWT } = require('../utils/jwt-validation')
const { registerApi } = require('../middleware/logger-api')

router.get('/hello', repartidor_controller.hello)
//using logs: router.get('/recibe', validateJWT,registerApi, repartidor_controller.recibe)
router.post('/recibe', validateJWT, repartidor_controller.recibe)
//using logs: router.post('/info', validateJWT, registerApi, repartidor_controller.info)
router.post('/info', validateJWT, repartidor_controller.info)
//using logs: router.post('/entrega', validateJWT, registerApi, repartidor_controller.entrega)
router.post('/entrega', validateJWT, repartidor_controller.entrega)
router.get('/getData', repartidor_controller.get_data)

router.post('/validateToken', validateJWT, repartidor_controller.token_ok)


module.exports = router