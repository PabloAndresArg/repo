var jwt = require('jsonwebtoken')
var data = []
const repartidor_controller = {}

repartidor_controller.hello = async (req,res) => {
    return res.status(200).send({ msg: 'hello world, repartidor 1' })
}

repartidor_controller.recibe = async (req,res) => {
    id_pedido = req.body.id_pedido
    if (!id_pedido) {
        console.log('error: no id')
        return res.status(401).json({ status: "error", msg: "There's no id" });
    }
    data.push({ id: id_pedido, state: 'recived'})
    console.log('order saved')
    console.log(data)
    return res.status(200).send({ msg: 'pedido recibido' })
}

repartidor_controller.info = async (req,res) => {
    id_pedido = req.body.id_pedido
    if (!id_pedido) {
        console.log('error: no id')
        return res.status(401).json({ status: "error", msg: "There's no id" });
    }
    try {
        console.log('looking for order: '+id_pedido)
        const order = data.find(order => order.id == id_pedido);
        return res.status(200).send({ state: order.state })
    } catch (error) {
        return res.send({ msg: "error order doesnt exist" });
    }
}

repartidor_controller.entrega = async (req,res) => {
    id_pedido = req.body.id_pedido
    if (!id_pedido) {
        console.log('error: no id')
        return res.status(401).json({ status: "error", msg: "There's no id" });
    }
    try {
        const order = data.find(order => order.id == id_pedido);
        order.state = 'delivered'
        console.log(data)
        return res.status(200).send({ msg: 'pedido entregado' })
    } catch (error) {
        return res.send({ msg: "error order doesnt exist" });
    }
}

repartidor_controller.get_data = async (req,res) => {
    console.log(data)
    return res.send(data);
}

repartidor_controller.token_ok = async (req,res) => {
    console.log('token ok')
    return res.status(200).send({ msg: 'token ok' })
}

module.exports = repartidor_controller