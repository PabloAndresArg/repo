import json
import jwt
import datetime
from flask import Flask, jsonify, request, Response

app = Flask(__name__)

@app.route('/login', methods=['POST'])
def newUser():
    print('LOGIN :)')
    data = request.get_json()
    username = data.get('username')
    tipo = data.get('tipo')
    tipo_valid = False
    ct = datetime.datetime.now()
    if (tipo == 'cliente' or tipo == 'restaurante' or tipo == 'repartidor'):
        token = jwt.encode({ 'user': username }, 'sa_practica1', algorithm="HS256")
        print(token)
        f = open("logs/login.log", "a")
        log = str(ct) + ": inicio de sesion\n"
        f.write(log)
        f.close()
        return Response(json.dumps({ 'msg': token }), status=200)
    else:
        f = open("logs/login.log", "a")
        log = str(ct) + ": error al iniciar sesion\n"
        f.write(log)
        f.close()
        return Response(json.dumps({ 'msg': 'user invalid' }), status=400)



if __name__ == '__main__':
    app.run(port = 2000, host='0.0.0.0',debug=True)