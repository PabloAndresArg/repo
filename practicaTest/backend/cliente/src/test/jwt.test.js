const httpMocks = require('node-mocks-http');
const expect = require('chai').expect;
const {validateJWT} = require('../middlewares/jwt-validation')

describe('jwt middleware ', function() {

    describe('sin token en los headers', function() {
        it('debería retornar un 401', function() {
            const req = httpMocks.createRequest({
                _parsedUrl: {
                    pathname: "/client/health"
                }
            });
            const res = httpMocks.createResponse();
            const next = function() {};

            validateJWT(req, res, next);
            expect(res.statusCode).to.equal(401);
        });

        it('should highlight proper menu item', function() {
            const req = httpMocks.createRequest({
                _parsedUrl: {
                    pathname: "/client/health"
                },
                headers: {
                    "x-auth-token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyIjoidGVzdCJ9.yVhgeB8gk-MH5MIFP8n1GBQu8GbFN2zZCG7YRF8WU-E"
                }
            });
            const res = httpMocks.createResponse();
            const next = function() {};

            validateJWT(req, res, next);
            expect(res.statusCode).to.equal(200);
        });
    })

});

