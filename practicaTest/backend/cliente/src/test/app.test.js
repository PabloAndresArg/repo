let chai = require("chai");
let chaiHttp = require("chai-http");
const app = require("../app");
const expect = require("chai").expect;
chai.use(chaiHttp);

// unit test #1 - healthy api

describe("1. health client", () => {
  it("show healthy message", (done) => {
    chai
      .request(app)
      .get("/client/health")
      .end(function (err, res) {
        if (err) console.log(err);
        expect(res).to.have.status(200);
        done();
      });
  });

  it("checkStateRestaurant", (done) => {
    chai
      .request(app)
      .get("/client/checkStateRestaurant")
      .set(
        "x-auth-token",
        "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyIjoidGVzdCJ9.yVhgeB8gk-MH5MIFP8n1GBQu8GbFN2zZCG7YRF8WU-E"
      )
      .end(function (err, res) {
        if (err) console.log(err);
        expect(res).to.have.status(400);
        done();
      });
  });

  it("checkStateDelivery", (done) => {
    chai
      .request(app)
      .get("/client/checkStateDelivery")
      .set(
        "x-auth-token",
        "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyIjoidGVzdCJ9.yVhgeB8gk-MH5MIFP8n1GBQu8GbFN2zZCG7YRF8WU-E"
      )
      .end(function (err, res) {
        if (err) console.log(err);
        expect(res).to.have.status(400);
        done();
      });
  });
});
