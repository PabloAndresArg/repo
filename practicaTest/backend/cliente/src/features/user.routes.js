const { Router } = require('express');
const router = Router();
const userCtrl = require('./user.controller');
const { validateJWT  } = require('../middlewares/jwt-validation');
const { registerApi } = require('../middlewares/logger-api');


router.get('/health', userCtrl.health);

router.post('/orderToRestaurant', validateJWT, registerApi, userCtrl.order);
router.get('/checkStateRestaurant', validateJWT, registerApi, userCtrl.checkStateRestaurant);
router.get('/checkStateDelivery', validateJWT, registerApi, userCtrl.checkStateDelivery);


module.exports = router;