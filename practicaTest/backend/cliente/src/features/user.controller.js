const  fetch = require('node-fetch') 
const userCtrl = {};
const { doRequest } = require("../utils/doRequest");


userCtrl.health = (req, res) => {
  return res.status(200).json({ status: "ok" });
};

userCtrl.order = async (req, res) => {
  const { dpi, producto } = req.body;
  const token = req.headers['x-auth-token'];

  const config = {
    url: `${process.env.RESTAURANTE_URL}/restaurant/RecibirPedidoDelCliente`,
    method: "post",
    data: {
      username: dpi,
      producto,
    },
    headers: {
      'x-auth-token': token
    },
  };

  const fetchResponse = await doRequest(config);
  if (!fetchResponse) return res.status(400).json({ status: "error" });

  const noOrden = { fetchResponse };
  return res.status(200).json({ noOrden });

};

userCtrl.checkStateRestaurant = async (req, res) => {
  const { noOrden } = req.query;
  const token = req.headers['x-auth-token'];

  const config = {
    url: `${process.env.RESTAURANTE_URL}/restaurant/InformarEstadoDelPedidoAlCliente/${noOrden}`,
    method: "get",
    data: {
        noOrden
    },
    headers: {
      'x-auth-token': token
    },
  };
  
  const fetchResponse = await doRequest(config);
  if (!fetchResponse) return res.status(400).json({ status: "error" });

  const { state } =  fetchResponse ;
  return res.status(200).json({ state });
  
};

userCtrl.checkStateDelivery = async (req, res) => {

  const { noOrden } = req.query;
  const token = req.headers['x-auth-token'];
  
  const config = {
    url: `${process.env.REPARTIDOR_URL}/repartidor/info`,
    method: "post",
    data: {
      id_pedido: noOrden,
      token: token,
    },
    headers: {
      'x-auth-token': token
    },
  };
  
  const fetchResponse = await doRequest(config);
  if (!fetchResponse) return res.status(400).json({ status: "error" });


  const { state } = fetchResponse;

    return res.status(200).json({ state });
  
};

module.exports = userCtrl;
