const jwt = require("jsonwebtoken");

const validateJWT = (req, res, next) => {
  console.log("***VALIDANDO***");
  const token = req.header("x-auth-token");
  console.log(token);
  if (!token)
    return res
      .status(401)
      .json({ status: "error", msg: "There's no token..." });

  try {
    const decoded = jwt.verify(token, "sa_practica1");
    console.log("*****");
    console.log(decoded);
  } catch (error) {
    console.log(error);
    return res.status(401).json({ status: "error", msg: "Token not valid." });
  }
  next();
};

module.exports = {
  validateJWT,
};
